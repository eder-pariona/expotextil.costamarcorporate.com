<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pasajes Aéreos con la Mejor Tarifa, compra en Costamar.com</title>
    <meta name="keywords" content="ctm, tours, peru, paquetes peru, programas, cusco, lima, destinos peru, machu picchu, colca, ctm tours peru, viajes peru, tours en peru, peru tours, operador, operator, peru tour operador, tour operador peru, receptivo peru, peru receptivo, lineas de nazca, cuzco, operador cusco, operador lima">
    <meta itemprop="name" content="Pasajes Aéreos con la Mejor Tarifa, compra en Costamar.com">
    <meta itemprop="description" content="Precio que ves, precio que pagas. Compra online con la mejor tarifa en Costamar.com">
    <meta name="keywords" content="pasajes baratos a,vuelos baratos a,mejor tarifa a,volar de,pasajes baratos a Cusco,pasajes baratos a Arequipa,pasajes baratos a Tumbes,pasajes baratos a Piura,pasajes baratos a Chiclayo,pasajes baratos a Trujillo,pasajes baratos a Lima,pasajes baratos a Tacna,pasajes baratos a Cajamarca,pasajes baratos a Puerto Maldonado,pasajes baratos a Juliaca,pasajes baratos a Iquitos,pasajes baratos a Huánuco,pasajes baratos a Jauja,vuelos baratos a Cusco,vuelos baratos a Arequipa,vuelos baratos a Tumbes,vuelos baratos a Piura,vuelos baratos a Chiclayo,vuelos baratos a Trujillo,vuelos baratos a Lima,vuelos baratos a Tacna,vuelos baratos a Cajamarca,vuelos baratos a Puerto Maldonado,vuelos baratos a Juliaca,vuelos baratos a Iquitos,vuelos baratos a Huánuco,vuelos baratos a Jauja,tickets baratos a Cusco,tickets baratos a Arequipa,tickets baratos a Tumbes,tickets baratos a Piura,tickets baratos a Chiclayo,tickets baratos a Trujillo,tickets baratos a Lima,tickets baratos a Tacna,tickets baratos a Cajamarca,tickets baratos a Puerto Maldonado,tickets baratos a Juliaca,tickets baratos a Iquitos,tickets baratos a Huánuco,tickets baratos a Jauja,volar de Lima a Cusco,volar de Lima a Arequipa,volar de Lima a Tumbes,volar de Lima a Piura,volar de Lima a Chiclayo,volar de Lima a Trujillo,volar de Lima a Lima,volar de Lima a Tacna,volar de Lima a Cajamarca,volar de Lima a Puerto Maldonado,volar de Lima a Juliaca,volar de Lima a Iquitos,volar de Lima a Huánuco,volar de Lima a Jauja,vuelos baratos a Arequipa,vuelos baratos a Cusco,vuelos baratos a Tumbes,vuelos baratos a Piura,vuelos baratos a Chiclayo,vuelos baratos a Trujillo,vuelos baratos a Lima,vuelos baratos a Tacna,vuelos baratos a Cajamarca,vuelos baratos a Puerto Maldonado,vuelos baratos a Juliaca,vuelos baratos a Iquitos,vuelos baratos a Huánuco,ofertas a Cusco,ofertas a Arequipa,ofertas a Tumbes,ofertas a Piura,ofertas a Chiclayo,ofertas a Trujillo,ofertas a Lima,ofertas a Tacna,ofertas a Cajamarca,ofertas a Puerto Maldonado,ofertas a Juliaca,ofertas a Iquitos,ofertas a Huánuco,ofertas a Jauja,Lan Perú,LATAM Airlines,Avianca,LC Perú,Star Perú,Peruvian Airlines">
    <!-- PARALLAX -->
    <link href='../css/demo.css' rel='stylesheet' type='text/css'>
    <script src="../jarallax/jarallax.js"></script>
    <script src="../jarallax/jarallax-video.js"></script>
    <!---->
    <link rel="stylesheet" href="../css/lightbox.min.css">
    <!---->
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../img/favicon.ico" rel="shortcut icon" type="../image/vnd.microsoft.icon">
    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- Plugin CSS -->
    <link href="../css/magnific-popup.css" rel="stylesheet">
    <!-- Theme CSS -->
    <link href="../css/creative.min.css" rel="stylesheet">
    <!-- MI -->
    <link href="../css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<style>
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    a {
        font-size: 13px;
    }

    .navbar-nav>li>a {
        line-height: 10px;
    }
</style>

</head>
<!--<body id="page-top">-->
<body>
    <!------------------------------------>
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top" style="z-index: 99;">
        <div class="container">
            <div class="navbar-header" style="float: left;">
                <a class="navbar-brand page-scroll" href="#page-top">
                    <img class="med-logo img-fluid" src="../img/LOGO-COSTAMAR.png">                         
                </a>
            </div>
            <div class="collapse navbar-collapse" style="float: right;">
                <ul class="nav navbar-nav navbar-right">
                    <a class="navbar-brand page-scroll top-sp" href="#page-top">
                        <img class="med-logo img-fluid" src="../img/logo_EXPOTEXTIL.png">                         
                    </a>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-inverse navbar-fixed-top spc-nav" style="z-index: 1;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 menu-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav alinear-der">
                                <li><a type="button" class="smoothScroll" href="#HT01">HOTEL MELIA LIMA</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT02">BUSINESS TOWER HOTEL - BTH</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT03">RAMADA ENCORE HOTEL</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT07">COSTA DEL SOL</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT13">LYZ HOTEL</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT12">HOTEL ESTELAR MIRAFLORES</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT04">JW MARRIOTT</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT05">COURTYARD LIMA</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT06">COUNTRY CLUB</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT08">SONESTA</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT09">JOSE ANTONIO DELUXE</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT10">JOSE ANTONIO</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT11">ANTARA HOTEL</a></li>
                                <li><a type="button" class="smoothScroll" href="#HT15">SOL DE ORO</a></li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="/en"><img width="20" src="../img/b_%E2%80%8Cing.png"> INGLES
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a type="button" class="smoothScroll" href="/"><img width="20" src="../img/b_esp.png"> ESPAÑOL</a></li>
                                    </ul>
                                </li>
                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div id="myCarousel" class="carousel slide carousel_principal">
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                            <li data-target="#myCarousel" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="../img/banner/banner01.jpg" style="width:100%" class="img-responsive">
                                <div class="container">
                                    <div class="carousel-caption">
                                        <h1>Lima</h1>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../img/banner/banner02.jpg" class="img-responsive">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Machu Picchu</h1>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../img/banner/banner03.jpg" class="img-responsive">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Trujillo</h1>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../img/banner/banner04.jpg" class="img-responsive">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Ica</h1>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../img/banner/banner05.jpg" class="img-responsive">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Iquitos</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <!-- FIN CAROUSEL -->
                <section class="bg-primary" id="about" style="padding-bottom: 50px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 text-center">
                                <h2 class="section-heading titulos-txt" style="color:#0067AB;">COSTAMAR TRAVEL</h2>
                                <hr class="light">
                                <p class="textos">
                                    <strong>Costamar Travel</strong>, Official Travel Agency of the event, was founded in 1980 in New Jersey, focusing on the Latin Market living in USA. Our main offices are in Peru and the United States, we also have offices in Mexico, Dominican Republic, Colombia, Ecuador and Brazil.<br>
                                    Our 1,300 employees are specialized in Corporate Travel, Events, Leisure Travel, and wholesaling to other agencies. In the last 3 years we have been granted the “World Travel Award” as the Leading Travel Agency in South America.<br>
                                    It will be our pleasure to handle your trip to Peru, we have special airfares, as well as tours in Lima and Pre and Post tours to discover Machu Picchu, the Nazca Lines, the Amazon Jungle and many other natural and cultural wonders of our country.<br>
                                    During the event we will have an Hospitality Desk at the venue, in order to facilitate you any last minute coordination of your visits and travel needs, although we recommend reserving them in advance to avoid any lack of space. Welcome to Perú.
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
                <?php include('packages.html') ?>
                <?php include('hotels.html') ?>
<!--                                         ->
<!-          FORMULARIO-PAQUETES             ->
<!-                                         -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="color:#546E7A;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel" style="text-align:center;">CONTACT FORM</h4>
            </div>
            <div class="modal-body" style="padding-bottom: 0px">
                <form id="contactUs" method="POST" action="../process.php">
                    <div class="form-group">
                        <strong style="color: red">*</strong> <span style="color: red">Required fields</span>
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="Nombre">Name & Last Name:</label>
                        <input required type="text" name="fullname" class="form-control" id="Nombre" placeholder="Name & Last Name" pattern="[A-Za-z-ñÑ áéíóú]+" title="Porfavor ingrese solo letras">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="Nombre">Email:</label>
                        <input required="" type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="Teléfono">Phone:</label>
                        <input required="" type="tel" name="phone" class="form-control" id="Teléfono" placeholder="Phone" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="Nacionalidad">Nationality:</label>
                        <input required="" name="nationality" type="text" class="form-control" id="Nacionalidad" placeholder="Nationality">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="tipoDOC">Document Type:</label>
                        <select required="" name="documentType" id="tipoDOC" class="form-control" placeholder="Document Type">
                            <option value="">Select Document</option>
                            <option value="carnet de extranjería">Foreigner's Card</option>
                            <option value="dni">DNI</option>
                            <option value="pasaporte">Pasaporte</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="numDOC">Document number:</label>
                        <input required="" type="text" name="documentNumber" class="form-control" id="numDOC" placeholder="Document number">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="tarjeta">Card Type:</label>
                        <select placeholder="Tarjeta" required="" name="cardType" id="tarjeta" class="form-control">
                            <option value="">Select Card</option>
                            <option value="visa">Visa</option>
                            <option value="mastercard">MasterCard</option>
                        </select>
                    </div>
                    <!-- visible solo para paquetes -->
                    <div class="form-group form-dates departureDateForm">
                        <strong style="color:red">*</strong>
                        <label for="">Start date :</label>
                        <input value ="18/10/2017" title="Start date" type="text" name="departureDateForm" 
                        class="form-control departureDate" maxlength="0">
                    </div>
                    <div class="form-group form-dates returnDateForm">
                        <strong style="color:red">*</strong>
                        <label for="">Final date :</label>
                        <input value="23/10/2017" title="Final date" type="text" name="returnDateForm" 
                        class="form-control returnDate" maxlength="0">
                    </div>
                    <div style="text-align: justify;line-height: 15px; margin-bottom: 9px">
                        <span class="tit-ter-txt">General condition:</span>
                        <br>
                        <span class="txt-ter-txt condiciones-nohotel">
                            Prices per person in a double room. Rate subject to availability and spaces at the time of booking. Non refundable, non endorsable and non-transferable. Does not include air fare. Subject to change. Does not apply to holidays or long weekend. Prices valid for foreigners. Valid until 15/12/2017.   
                        </span>
                        <span class="txt-ter-txt condiciones-hotel">
                            - Rates for foreign passengers.<br>
                            - Rates in US dollars.<br>
                            - Rates per room per night in single / double room.<br>
                            - Rates include Breakfast at the hotel.<br>
                            - Rates include 10% hotel services.<br>
                            - Rates DO NOT include 18% IGV only for Peruvian passengers or residents.<br>
                            - Passengers who do not present the Andean Immigration Card must assume 18% of IGV.<br>
                            - Not valid for passengers with more than 60 days of stay in Peru.
                        </span>
                    </div>
                    <div class="form-group">
                        <a class="terms" style="cursor: pointer; color: #546E7A !important; font-weight: bold !important;" data-toggle="modal" data-target="#modalPDF">Click to see the Terms and Conditions
                        </a>
                    </div>
                    <div class="checkbox">
                        <strong style="color:red">*</strong>
                        <label>
                            <input required="" type="checkbox"> I accept the terms and conditions.
                        </label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <div class="g-recaptcha-response">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div id="recaptcha" class="g-recaptcha" data-callback="capcha_filled" data-expired-callback="capcha_expired">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button id="booking_now" type="submit" class="btn btn-primary btn_reservar"><i class="fa fa-check-circle" aria-hidden="true"></i> BOOK NOW</button>
                    </div>
                    <input type="hidden" name="request">
                    <input type="hidden" name="typePackage">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Close</button>
            </div>
        </div>
    </div>
</div>
<a href="#" class="scroll-to-top">
    <span class="glyphicon glyphicon-arrow-up" style="color: #FFFFFF"></span>
</a>
<!--
<div class="jarallax frase-txt" data-jarallax='{"speed": 0.2}' style="background-image: url(img/fondo02.png);"></div>
-->
<section id="contact" style="position: relative;background-color: #48321f;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 pie-footer-txt">
                © Costamar Travel 2017. All rights reserved
            </div>
        </div>
    </div>
</section>
<!-- jQuery -->
<script src="../js/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>
<!-- CALENDARIO -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $("#datepicker").datepicker();
    });
</script>
<script>
    $(function() {
        $("#datepicker2").datepicker();
        $("#datepicker3").datepicker();
    });
</script>
<script>
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        autoSize: true,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(function() {
        $("#datepicker").datepicker();
    });
</script>
<script>
    (function($) {

        $(document).ready(

            function() {

// Comprobar si estamos, al menos, 100 px por debajo de la posición top
// para mostrar o esconder el botón
$(window).scroll(function() {

    if ($(this).scrollTop() > 100) {

        $('.scroll-to-top').fadeIn();

    } else {

        $('.scroll-to-top').fadeOut();

    }

});

// al hacer click, animar el scroll hacia arriba
$('.scroll-to-top').click(function(e) {

    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, 800);

});

});

    })(jQuery);
</script>
<!---------------------------------->
<!-- SCROOLL -->
<script src="../js/jquery.smooth-scroll.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('a.smoothScroll').smoothScroll({
            offset: -80,
            scrollTarget: $(this).val()
        });
    });
</script>
<!---->
<script src="../js/lightbox.min.js"></script>
<script>
    lightbox.option({
        'albumLabel': "Imagen %1 de %2"
    })
</script>
<script src="../js/main.js"></script>
<div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="color:#546E7A;">
    <div class="modal-dialog" role="document" id="modalPDFContent">
        <div class="modal-content">
            <div class="modal-body">
                <iframe src="http://porcicultura.costamarcorporate.com/terminos-y-condiciones-costamar.pdf" width="100%" height="600"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
            </div>
        </div>
    </div>
</div>
</body>

</html>