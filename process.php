<?php 
if($_POST) {
	$request = $_POST;
	if (isset($_POST['fullname'])) {
		if (isset($_POST['fullname'])) {
			if (!$_POST["recaptcha_challenge_field"]) {
				echo '{"code":400,"message":"ERROR: Este formulario no es valido"}';
				exit();
			}
			/*Validacion captcha*/
			require_once 'google/1.11/recaptchalib.php';
			$secret = "6LeHHCsUAAAAAKE1FMkW29MNvAj9-vQz6H44OqO1";
			$response = recaptcha_check_answer(
												$secret,
												$_SERVER['REMOTE_ADDR'],
												$_POST['recaptcha_challenge_field'],
												$_POST['recaptcha_response_field']
											);
			if (!$response->is_valid) {
				$message = $response->error;
				if ($response->error == 'incorrect-captcha-sol') {
					$message = 'El captcha es incorrecto';
				}
				header('Content-Type: application/json');
				echo '{"code":400,"message":"ERROR: '.$message.'"}';
				exit();
			}

			$typePackage = $_POST['typePackage'];
			if( $typePackage == 'H' ) {
				$preRequest             = json_decode($_POST['request']);
				$_POST['departureDate'] = $preRequest->departureDate;
				$_POST['returnDate']    = $preRequest->returnDate;
				$_POST['travelType']	= 'H';
			} else if($typePackage == 'TD') {
				$_POST['departureDate'] = $_POST['departureDateForm'];
				$_POST['returnDate']    = $_POST['returnDateForm'];
				$_POST['travelType']    = 'TD';

			} else if($typePackage == 'T') {
				$_POST['departureDate'] = $_POST['departureDateForm'];
				$_POST['travelType']	= 'T';
			} else {
				exit('-');
			}
			
			unset($_POST['recaptcha_challenge_field']);
			unset($_POST['recaptcha_response_field']);
			unset($_POST['departureDateForm']);
			unset($_POST['returnDateForm']);
			unset($_POST['typePackage']);


			/*Ordenar la data antes de enviar*/
			$request = $_POST;
			$data = json_encode($_POST);
			
			/*llamar al servicio*/
			/*prod: 208.78.163.68*/
			$url = 'http://208.78.163.68/app_dev.php/api/contacts';
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json',
			    'Content-Length: ' . strlen($data)
			    )
			);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($curl);
			header('Content-Type: application/json');
			echo $result;
		}
	}
}

function _clean($str){
	return is_array($str) ? array_map('_clean', $str) : str_replace('\\', '\\\\', strip_tags(trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES))));
}

