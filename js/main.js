$( function(){
	var 
		$returnDate          = $( ".returnDate" ),
		$departureDate       = $( ".departureDate" ),
		$rooms               = $( "select.rooms" ),
		$radios              = $( "input:radio" ),
		departureDateDefault = '18/10/2017',
		returnDateDefault    = '23/10/2017'
	;
	/*
	var currentDate = getDate(3);
	$('input.departureDate').val(currentDate);
	var currentDate = getDate(2);
	$('input.returnDate').val(currentDate);
	*/
	$('input.departureDate').val(departureDateDefault);
	$('input.returnDate').val(returnDateDefault);

	function calculateDaysInRangeDates(object) 
	{
		var 
			c            = 24*60*60*1000,
			kind         = $(object).attr('class'),
			isDeparture  = false,
			isReturnDate = false,
			a            = false,
			b            = false,
			request      = {}
		;

		if ( kind.indexOf('departureDate') >= 0) {
			isDeparture = true;
		} else if (kind.indexOf('returnDate') >= 0) {
			isReturnDate = true;
		} else {
			return false;
		}

		if( isDeparture ){
			b       = $(object).next().val();
			a       = $(object).parent().next().children('input.timeValue').val()
			$rooms  = $(object).parent().next().next().children('select');
			$radios = $(object).parent().next().next().next().children('div.radio').children('label').children('input:checked');
			
		} else if( isReturnDate ) {
			a       = $(object).next().val();
			b       = $(object).parent().prev().children('input.timeValue').val();
			$rooms  = $(object).parent().next().children('select');
			$radios = $(object).parent().next().next().children('div.radio').children('label').children('input:checked');
		}
		
		if ( a && b ) {
			diffDays  = Math.round(Math.abs((a - b)/(c)));
			$(object).parent().parent().children('input.diffDays').val(diffDays);
			$diffDays = $(object).parent().parent().children('input.diffDays');
			calculatePriceFinal($rooms, $radios, $diffDays);
		}
	};

	var calculatePriceFinal = function ($rooms, $radios, $diffDays)
	{
		var
			room       = parseFloat($rooms.val()),
			days       = parseFloat($diffDays.val()),
			priceMain  = 0,
			priceFinal = 0,
			preRequest = {}
		;
		priceMain = room * days;
		transfer  = 0;
		if( $radios.val() && $radios.val() != "" ) {
			transfer = parseFloat($radios.val());
		}
		priceFinal               = priceMain + transfer;
		priceFinalDecimal        = priceFinal.toFixed(2); 
		$rooms.parent().parent().children('div.prices').children('strong').text("");
		$rooms.parent().parent().children('div.prices').children('strong').text("USD " + priceFinalDecimal);
		preRequest.priceFinal    = priceFinalDecimal;
		preRequest.roomText      = $rooms.find('option:selected').text().trim();
		preRequest.roomValue     = $rooms.val();
		preRequest.transferValue = $radios.val();
		preRequest.transferText  = $radios.parent().text().trim();
		preRequest.departureDate = $rooms.parent().prev().prev().children('input[name=departureDate]').val();
		preRequest.returnDate    = $rooms.parent().prev().children('input[name=returnDate]').val();
		preRequest.description   = $rooms.parent().parent().parent().prev().children().text().trim();
		preRequest.tittle        = $rooms.parent().parent().parent().parent().parent().parent().parent().prev('div').children().text();
		$rooms.parent().parent().children('input[name=pre_request]').val(JSON.stringify(preRequest));
	};

	var minDate = $departureDate.val();
	var maxDate = $returnDate.val();

	$departureDate.datepicker({
		setDate: "+0d",
		gotoCurrent: true,
		minDate: minDate,		
		defaultDate: "+1w",
		numberOfMonths: 2,
		maxDate: maxDate
	  }).on('change', function(){
		var date1    = $(this).datepicker('getDate');
		date1.setDate(date1.getDate()+1);
		$returnDate.datepicker("option", "minDate", date1);
		var dateTime = $(this).datepicker('getDate').getTime()
		$(this).next().val(dateTime);
		calculateDaysInRangeDates(this);
	  })
	;
	$returnDate.datepicker({
		defaultDate: "+1w",
		selectDefaultDate: true,
		numberOfMonths:2,
		maxDate: maxDate
	  }).on('change', function(){
	 	var dateTime = $(this).datepicker('getDate').getTime()
		$(this).next().val(dateTime);
		calculateDaysInRangeDates(this);
	  });

	$rooms.on('change', function(){
		var 
			$diffDays = $(this).parent().parent().children('input.diffDays'),
			$radios   = $(this).parent().next().children('div.radio').children('label').children('input:checked');
		
		;
		if( $diffDays.val() != ""){
			calculatePriceFinal($(this), $radios, $diffDays);
		}
	});

	$radios.on('change', function(){
		var 
			$diffDays = $(this).parent().parent().parent().parent().children('input.diffDays'),
			$rooms    = $(this).parent().parent().parent().prev().children('select')
		;
		if( $diffDays.val() != ""){
			calculatePriceFinal($rooms, $(this), $diffDays);
		}
	})

	/*Fechas por default al cargar la pagina*/
	$('input.returnDate').trigger('change');
	$('input.departureDate').trigger('change');

	$("button.btn.btn-primary.btn-lg").click(function()
	{
		/* $('form#contactUs').trigger("reset"); */
		var proceed = $(this).prev().children('div.prices').children('strong').text();
		$('input[name=request]').val("");
		$("input[name=recaptcha_response_field]").prop('required',true);
		$('input.returnDate').val(returnDateDefault);
		if( $(this).hasClass('hotel') ) {
			if (proceed.trim() != 	"??.??") {
				$('div.form-dates').hide();
				$("input[name=departureDateForm]").prop('required',false);
				$("input[name=returnDateForm]").prop('required',false);
				var preRequest = $(this).prev().children('input[name=pre_request]').val();
				$('input[name=request]').val(preRequest);
				$('input[name=typePackage]').val('H');
			} else {
				return false;
			}
			/* Mostrar las condiciones generales de hotel */
			$('span.condiciones-nohotel').hide();
			$('span.condiciones-hotel').show();

		} else {
			/*es un paquete por dias, mostrar ambas fechas*/
			if ($(this).hasClass('tour-by-days')) {
				$('input[name=typePackage]').val('TD');
				$('div.form-dates.departureDateForm').show();				
				$('div.form-dates.returnDateForm').show();
				$("input[name=returnDateForm]").prop('required',true);				
			} else {
				/* es un paquete solo mostrar fecha de inicio*/
				$('input[name=typePackage]').val('T');
				$('div.form-dates.returnDateForm').hide();
				$("input[name=returnDateForm]").prop('required',false);
				$("input[name=returnDateForm]").val('');			
				$('div.form-dates.departureDateForm').show();				
			}
			/* fecha de inicio siempre es requerido */
			$("input[name=departureDateForm]").prop('required',true);
			var 
				preRequest         = {},
				priceFinal         = $(this).parent().children('span[class^="txt-precio"]').text()
			;

			preRequest.priceFinal  = priceFinal.replace('$', '').trim();
			preRequest.tittle      = $(this).parent().prev().children('h2').text();
			preRequest.subtittle   = $(this).parent().prev().children('h3').text();
			preRequest.description = $(this).parent().prev().children('span').text();
			$('input[name=request]').val("");
			$('input[name=request]').val(JSON.stringify(preRequest));
			
			/* Mostrar las condiciones generales de paquetes */
			$('span.condiciones-nohotel').show();
			$('span.condiciones-hotel').hide();
		}

		Recaptcha.reload();
	});

	$('form#contactUs').submit(function(event)
	{
		event.preventDefault();
		var 
			$form = $( this ),
			$button = $( this ).find('button')
		;
		$button.find('i').attr('class', 'fa fa-refresh fa-spin');
		$.ajax({
			type    	: 'POST', 
			url     	: $form.attr( "action" ), 
			data    	: $form.serialize(), 
			dataType  	: 'json', 
			encode    	: true
		}).done(function(data){
			$button.find('i').attr('class', 'fa fa-check-circle');
			Recaptcha.reload();
		  	alert(data.message);
			if (data.code == 200){
		  		setCloseModal();
			}
		}).fail(function(data){
			$button.find('i').attr('class', 'fa fa-check-circle');
		  	console.log(data);
			Recaptcha.reload();
			setCloseModal();
		})
	});

	/*Recaptcha*/
	Recaptcha.create('6LeHHCsUAAAAAFa2PgwgrMuqVjsQFRjCs9RkoTwP', 'recaptcha', {
		theme: 'white',
	});

	$('div#modalPDFContent').css('width', '70%');
});

function setCloseModal() 
{
	$('#myModal').trigger('click.dismiss.bs.modal') 
}

function getDate(day)
{
	var today = new Date();
	today.setDate(today.getDate() + day);
	var dd = today.getDate();
	var mm = today.getMonth() + 2; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	var today = dd+'/'+mm+'/'+yyyy;

	return today;
}
