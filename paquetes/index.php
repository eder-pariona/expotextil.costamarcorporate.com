<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pasajes Aéreos con la Mejor Tarifa, compra en Costamar.com</title>
    <meta name="keywords" content="ctm, tours, peru, paquetes peru, programas, cusco, lima, destinos peru, machu picchu, colca, ctm tours peru, viajes peru, tours en peru, peru tours, operador, operator, peru tour operador, tour operador peru, receptivo peru, peru receptivo, lineas de nazca, cuzco, operador cusco, operador lima">
    <meta itemprop="name" content="Pasajes Aéreos con la Mejor Tarifa, compra en Costamar.com">
    <meta itemprop="description" content="Precio que ves, precio que pagas. Compra online con la mejor tarifa en Costamar.com">
    <meta name="keywords" content="pasajes baratos a,vuelos baratos a,mejor tarifa a,volar de,pasajes baratos a Cusco,pasajes baratos a Arequipa,pasajes baratos a Tumbes,pasajes baratos a Piura,pasajes baratos a Chiclayo,pasajes baratos a Trujillo,pasajes baratos a Lima,pasajes baratos a Tacna,pasajes baratos a Cajamarca,pasajes baratos a Puerto Maldonado,pasajes baratos a Juliaca,pasajes baratos a Iquitos,pasajes baratos a Huánuco,pasajes baratos a Jauja,vuelos baratos a Cusco,vuelos baratos a Arequipa,vuelos baratos a Tumbes,vuelos baratos a Piura,vuelos baratos a Chiclayo,vuelos baratos a Trujillo,vuelos baratos a Lima,vuelos baratos a Tacna,vuelos baratos a Cajamarca,vuelos baratos a Puerto Maldonado,vuelos baratos a Juliaca,vuelos baratos a Iquitos,vuelos baratos a Huánuco,vuelos baratos a Jauja,tickets baratos a Cusco,tickets baratos a Arequipa,tickets baratos a Tumbes,tickets baratos a Piura,tickets baratos a Chiclayo,tickets baratos a Trujillo,tickets baratos a Lima,tickets baratos a Tacna,tickets baratos a Cajamarca,tickets baratos a Puerto Maldonado,tickets baratos a Juliaca,tickets baratos a Iquitos,tickets baratos a Huánuco,tickets baratos a Jauja,volar de Lima a Cusco,volar de Lima a Arequipa,volar de Lima a Tumbes,volar de Lima a Piura,volar de Lima a Chiclayo,volar de Lima a Trujillo,volar de Lima a Lima,volar de Lima a Tacna,volar de Lima a Cajamarca,volar de Lima a Puerto Maldonado,volar de Lima a Juliaca,volar de Lima a Iquitos,volar de Lima a Huánuco,volar de Lima a Jauja,vuelos baratos a Arequipa,vuelos baratos a Cusco,vuelos baratos a Tumbes,vuelos baratos a Piura,vuelos baratos a Chiclayo,vuelos baratos a Trujillo,vuelos baratos a Lima,vuelos baratos a Tacna,vuelos baratos a Cajamarca,vuelos baratos a Puerto Maldonado,vuelos baratos a Juliaca,vuelos baratos a Iquitos,vuelos baratos a Huánuco,ofertas a Cusco,ofertas a Arequipa,ofertas a Tumbes,ofertas a Piura,ofertas a Chiclayo,ofertas a Trujillo,ofertas a Lima,ofertas a Tacna,ofertas a Cajamarca,ofertas a Puerto Maldonado,ofertas a Juliaca,ofertas a Iquitos,ofertas a Huánuco,ofertas a Jauja,Lan Perú,LATAM Airlines,Avianca,LC Perú,Star Perú,Peruvian Airlines">
    <!-- PARALLAX -->
    <link href='../css/demo.css' rel='stylesheet' type='text/css'>
    <script src="../jarallax/jarallax.js"></script>
    <script src="../jarallax/jarallax-video.js"></script>
    <!---->
    <link rel="stylesheet" href="../css/lightbox.min.css">
    <!---->
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../img/favicon.ico" rel="shortcut icon" type="../image/vnd.microsoft.icon">
    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- Plugin CSS -->
    <link href="../css/magnific-popup.css" rel="stylesheet">
    <!-- Theme CSS -->
    <link href="../css/creative.min.css" rel="stylesheet">
    <!-- MI -->
    <link href="../css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
</head>
<!--<body id="page-top">-->
<body>
    <!------------------------------------>
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top" style="z-index: 99;">
        <div class="container">
            <div class="navbar-header" style="float: left;">
                <a class="navbar-brand page-scroll" href="#page-top">
                    <img class="med-logo img-fluid" src="../img/LOGO-COSTAMAR.png">                         
                </a>
            </div>
            <div class="collapse navbar-collapse" style="float: right;">
                <ul class="nav navbar-nav navbar-right">
                    <a class="navbar-brand page-scroll top-sp" href="#page-top">
                      <img class="med-logo img-fluid" src="../img/logo_EXPOTEXTIL.png">                         
                  </a>
              </ul>
          </div>
      </div>
  </nav>
  <nav class="navbar navbar-inverse navbar-fixed-top spc-nav" style="z-index: 1;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 menu-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav alinear-der">
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">CUSCO <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a type="button" class="smoothScroll" href="#TOUR01">CUSCO CON MACHU PICCHU</a></li>
                                    <li><a type="button" class="smoothScroll" href="#TOUR02">CUSCO CON MACHU PICCHU Y CITY</a></li>
                                    <li><a type="button" class="smoothScroll" href="#TOUR03">CUSCO COMPLETO</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">LIMA
                                    <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a type="button" class="smoothScroll" href="#TOUR04">LIMA EXPERIENCIA GASTRONÓMICA</a></li>
                                        <li><a type="button" class="smoothScroll" href="#TOUR05">LIMA CITY TOUR</a></li>
                                        <li><a type="button" class="smoothScroll" href="#TOUR06">LIMA ILUMINA</a></li>
                                        <li><a type="button" class="smoothScroll" href="#TOUR07">PACHACAMAC CIUDADELA SAGRADA</a></li>
                                        <li><a type="button" class="smoothScroll" href="#TOUR08">MUSEO DE ORO</a></li>
                                        <li><a type="button" class="smoothScroll" href="#TOUR09">MUSEO LARCO HERRERA</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">ICA Y PARACAS
                                        <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a type="button" class="smoothScroll" href="#TOUR10">LA HUACACHINA Y SOBREVUELO A LAS LINEAS DE NAZCA FULL DAY</a></li>
                                            <li><a type="button" class="smoothScroll" href="#TOUR11">PARACAS Y SOBREVUELO A LAS LINEAS DE NAZCA</a></li>
                                            <li><a type="button" class="smoothScroll" href="#TOUR12">PARACAS, ICA Y SOBREVUELO A LAS LINEAS DE NAZCA</a></li>
                                        </ul>
                                    </li>
                                    <li><a type="button" class="smoothScroll" href="#TOUR13">TRUJILLO Y CHICLAYO</a></li>
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">SELVA
                                            <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a type="button" class="smoothScroll" href="#TOUR14">PUERTO MALDONADO</a></li>
                                                <li><a type="button" class="smoothScroll" href="#TOUR14">IQUITOS - CEIBA TOPS</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="../paquetes/index.html"><img width="20" src="../img/b_esp.png"> ESPAÑOL
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a type="button" class="smoothScroll" href="../packages/index.html"><img width="20" src="../img/b_%E2%80%8Cing.png"> INGLES</a></li>
                                            </ul>
                                        </li>
                                            
                                    </ul>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <div id="myCarousel" class="carousel slide carousel_principal">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="../img/banner/banner01.jpg" style="width:100%" class="img-responsive">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Lima</h1>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="../img/banner/banner02.jpg" class="img-responsive">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Machu Picchu</h1>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="../img/banner/banner03.jpg" class="img-responsive">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Trujillo</h1>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="../img/banner/banner04.jpg" class="img-responsive">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Ica</h1>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="../img/banner/banner05.jpg" class="img-responsive">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Iquitos</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                 <span class="glyphicon glyphicon-chevron-left"></span>
                 <span class="sr-only">Previous</span>
             </a>
             <a class="right carousel-control" href="#myCarousel" data-slide="next">
                 <span class="glyphicon glyphicon-chevron-right"></span>
                 <span class="sr-only">Next</span>
             </a>
         </div>
         <!-- FIN CAROUSEL -->
         <section class="bg-primary" id="about" style="padding-bottom: 50px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12 text-center">
                        <h2 class="section-heading titulos-txt" style="color:#0067AB;">COSTAMAR TRAVEL</h2>
                        <hr class="light">
                        <p class="textos">
                            <strong>Costamar Travel</strong>, Agencia de Viajes Oficial, fue fundada en 1980 en <strong>Nueva Jersey</strong>, enfocándose en los viajeros latinos de los <strong>Estados Unidos</strong>. Nuestras oficinas principales están en Perú y Estados Unidos, también contamos con oficinas en México, República Dominicana, Colombia, Ecuador y Brasil.
                            <br> Nuestros 1.300 colaboradores laboran en las áreas de Viajes de Placer, Viajes Corporativos, Organización de Eventos y venta por mayor. En los últimos 3 años hemos sido acreedores del World Travel Award como la agencia líder en Sudamérica.
                            <br> Será un gusto atender su viaje a Perú, tenemos tarifas aéreas especiales, así como tours en Lima y Pre y post tours para conocer Machu Picchu y otras bellezas de nuestro país.
                            <br> Durante el evento tendremos un Mostrador de Hospitalidad en la sede del evento, a fin de atender todas las últimas coordinaciones de sus tours y viajes, aunque recomendamos reservarlos con anticipación.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- -->
        <div class="tit_serv">
            <h2 class="tit-ciudad" style="color: #FFFFFF;"><i class="fa fa-suitcase" aria-hidden="true"></i> PAQUETES</h2>
        </div>
        <!-- -->
        <!-- -->
        <div class="demo-gap">
            <h2 class="tit-ciudad">CUSCO</h2>
        </div>
        <div id="TOUR01" class="jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url(../img/paquetes/cusco01.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h2 class="txt-tit">CUSCO CON MACHU PICCHU</h2>
                        <h3 class="txt-subTIT"><i class="fa fa-sun-o" aria-hidden="true"></i> 03DIAS / <i class="fa fa-moon-o" aria-hidden="true"></i> 02NOCHES</h3>
                        <br>
                        <span class="txt-desc">
                           <i class="fa fa-check" aria-hidden="true"></i> Traslado aeropuerto/hotel/aeropuerto.<br>
                           <i class="fa fa-check" aria-hidden="true"></i> 2 noches de alojamiento en Cusco (02 desayunos).<br>
                           <i class="fa fa-check" aria-hidden="true"></i> FD Machu Picchu en servicio compartido y tren Expedition (almuerzo incluido).<br>
                           <i class="fa fa-check" aria-hidden="true"></i> Transporte, entradas y guiado en servicio regular.
                       </span>
                   </div>
                   <div class="col-xs-12 col-md-6 pos-precio">
                    <span class="txt-desde">Precio por persona <strong>DESDE</strong></span>
                    <br>
                    <span class="txt-precio">$ 459.00</span>
                    <br>
                    <button type="button" class="btn btn-primary btn-lg tour-by-days" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="TOUR02" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #a0836f;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h2 class="txt-tit-02">CUSCO CON MACHU PICCHU CITY</h2>
                    <h3 class="txt-subTIT-02"><i class="fa fa-sun-o" aria-hidden="true"></i> 03DIAS / <i class="fa fa-moon-o" aria-hidden="true"></i> 02NOCHES</h3>
                    <br>
                    <span class="txt-desc-02">
                       <i class="fa fa-check" aria-hidden="true"></i> Traslado aeropuerto/hotel/aeropuerto.<br>
                       <i class="fa fa-check" aria-hidden="true"></i> 2 noches de alojamiento en Cusco (02 desayunos).<br>
                       <i class="fa fa-check" aria-hidden="true"></i> HD City Tour Cusco & Parque Arqueológico de Sacsayhuaman.<br>
                       <i class="fa fa-check" aria-hidden="true"></i> FD Machu Picchu en servicio compartido y tren Expedition (almuerzo incluido).<br>
                       <i class="fa fa-check" aria-hidden="true"></i> Transporte, entradas y guiado en servicio regular.
                   </span>
               </div>
               <div class="col-xs-12 col-md-6 pos-precio">
                <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
                <br>
                <span class="txt-precio-02">$ 522.00</span>
                <br>
                <button type="button" class="btn btn-primary btn-lg tour-by-days" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
                </button>
            </div>
        </div>
    </div>
</div>
<div id="TOUR03" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #8D6E63;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">CUSCO COMPLETO</h2>
                <h3 class="txt-subTIT-02"><i class="fa fa-sun-o" aria-hidden="true"></i> 04DIAS / <i class="fa fa-moon-o" aria-hidden="true"></i> 03NOCHES</h3>
                <br>
                <span class="txt-desc-02">
                   <i class="fa fa-check" aria-hidden="true"></i> Traslado aeropuerto/hotel/aeropuerto.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 2 noches de alojamiento en Cusco (02 desayunos).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 1 noche de alojamiento en Valle Sagrado (01desayuno)<br>
                   <i class="fa fa-check" aria-hidden="true"></i> HD City Tour Cusco & Parque Arqueológico de Sacsayhuaman.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> FD Valle Sagrado de los Incas (almuerzo incluido).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> FD Machu Picchu en servicio compartido y tren Expedition (almuerzo incluido).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Transporte, entradas y guiado en servicio regular.
               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 652.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg tour-by-days" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div class="demo-gap">
    <h2 class="tit-ciudad">LIMA</h2>
</div>
<div id="TOUR04" class="jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url(../img/paquetes/lima01.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit">LIMA EXPERIENCIA GASTRONÓMICA</h2>
                <span class="txt-desc">
                    <i class="fa fa-check" aria-hidden="true"></i> Transporte<br>
                    <i class="fa fa-check" aria-hidden="true"></i> Visita a un típico mercado modelo peruano.<br>
                    <i class="fa fa-check" aria-hidden="true"></i> Visita a un restaurante exclusivo donde se preparará Pisco Sour y Ceviche.<br>
                    <i class="fa fa-check" aria-hidden="true"></i> Almuerzo incluido.<br>
                    <i class="fa fa-check" aria-hidden="true"></i> Duración: 4 horas.
                </span>
            </div>
            <div class="col-xs-12 col-md-6 pos-precio">
                <span class="txt-desde">Precio por persona <strong>DESDE</strong></span>
                <br>
                <span class="txt-precio">$ 90.00</span>
                <br>
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
                </button>
            </div>
        </div>
    </div>
</div>
<div id="TOUR05" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #a0836f;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">LIMA CITY TOUR</h2>
                <span class="txt-desc-02">
                  Nuestro tour presenta Lima en sus 3 periodos históricos.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Ingreso al Museo BCR, Complejo Monumental San Francisco y su Catacumbas.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Guía turístico en inglés o español.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Transporte desde hoteles en Lima y Miraflores.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Duración: 3 1/2 horas.
              </span>
          </div>
          <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 40.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div id="TOUR06" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #8D6E63;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">LIMA ILUMINADA</h2>
                <span class="txt-desc-02">
                  Nuestra excursión muestra en detalle el imponente Centro Histórico de Lima, la Plaza San Martin, la Catedral de Lima y otros lugares. Además, podrá disfrutar del espectáculo de agua, luz, música e imágenes, presentadas en el famoso Parque de la Reserva.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Guía turístico en inglés o español.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Transporte desde hoteles en Lima y Miraflores.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Duración: 3 1/2 horas.<br>
                  <strong>SALIDAS:</strong> De miércoles a domingo.
              </span>
          </div>
          <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 44.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div id="TOUR07" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #a0836f;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">PACHACAMAC CIUDADELA SAGRADA</h2>
                <span class="txt-desc-02">
                  Una experiencia arqueológica y mística inolvidable.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Visita guiada a la espléndida Ciudadela de Pachacamac, Santuario del Dios Pachacamac, Templo del Sol, Templo de Pachacamac, Acllahuasi o Palacio de las Vírgenes del Sol.<br>
                  <i class="fa fa-check" aria-hidden="true"></i> Duración: 3 1/2 horas.<br>
                  <strong>SALIDAS:</strong> De martes a domingo. 

              </span>
          </div>
          <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 49.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div id="TOUR08" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #8D6E63;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">MUSEO DE ORO</h2>
                <span class="txt-desc-02">
                   <i class="fa fa-check" aria-hidden="true"></i> Exhibición de los más finos objetos trabajados en oro y plata del antiguo Perú, así como la colección más variada de armas de diferentes lugares del mundo en el “Museo de Oro del Perú”.<br>
                   <strong>SALIDAS:</strong> De miércoles a domingo.
               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 56.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div id="TOUR09" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #a0836f;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">MUSEO LARCO HERRERA</h2>
                <span class="txt-desc-02">
                   <i class="fa fa-check" aria-hidden="true"></i> Descubra los impresionantes tesoros del antiguo Perú, donde se exhibe la más notable colección de oro y plata del Perú precolombino en el mundo; así como su famosa colección de cerámica erótica.<br>
                   <strong>SALIDAS:</strong> De martes a domingo. 

               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 45.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div class="demo-gap">
    <h2 class="tit-ciudad">ICA PARACAS</h2>
</div>
<div id="TOUR10" class="jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url(../img/paquetes/ica01.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit">LA HUACACHINA Y SOBREVUELO A LAS LINEAS DE NAZCA FULL DAY</h2>
                <span class="txt-desc">
                   <i class="fa fa-check" aria-hidden="true"></i> Traslados desde/hasta el hotel.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Transporte en bus turístico Lima/Ica/Lima.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Desayuno abordo tipo snack.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Visita a la Laguna de La Huacachina.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Sobrevuelo desde Ica (Incluye Impuestos).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Almuerzo en restaurante local de Ica.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Guiado en servicio regular (español o inglés). 
               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio">$ 355.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div id="TOUR11" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #a0836f;padding-top:25px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">PARACAS Y SOBREVUELO A LAS LINEAS DE NAZCA</h2>
                <h3 class="txt-subTIT-02"><i class="fa fa-sun-o" aria-hidden="true"></i> 02DIAS / <i class="fa fa-moon-o" aria-hidden="true"></i> 01NOCHES</h3>
                <span class="txt-desc-02">                    
                   <i class="fa fa-check" aria-hidden="true"></i> Traslados desde/hasta el hotel.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Transporte en bus turístico Lima/Paracas/Lima.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 01 noche de alojamiento en Paracas (01 desayuno).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Sobrevuelo a las líneas de Nazca (desde Pisco).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Excursión Marítima a las Islas Ballestas.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Guiado en servicio regular (español - inglés). 
               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 542.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg tour-by-days" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div id="TOUR12" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #8D6E63;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">PARACAS, ICA Y SOBREVUELO A LAS LINEAS DE NAZCA</h2>
                <h3 class="txt-subTIT-02"><i class="fa fa-sun-o" aria-hidden="true"></i> 03DIAS / <i class="fa fa-moon-o" aria-hidden="true"></i> 02NOCHES</h3>
                <span class="txt-desc-02">    
                   <i class="fa fa-check" aria-hidden="true"></i> Traslados desde/hasta el hotel.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Transporte en bus turístico Lima/Paracas/Ica/Paracas/Lima.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 02 noches de alojamiento en Paracas (02 desayunos).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Sobrevuelo a las líneas de Nazca (desde Pisco).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> HD City Tour Ica + Areneros y Sand boarding.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Excursión Marítima a las Islas Ballestas.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Guiado en servicio regular (español - inglés). 
               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 747.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg tour-by-days" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div class="demo-gap">
    <h2 class="tit-ciudad">TRUJILLO Y CHICLAYO</h2>
</div>
<div id="TOUR13" class="jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url(../img/paquetes/trujillo01.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit">TRUJILLO Y CHICLAYO</h2>
                <h3 class="txt-subTIT-02"><i class="fa fa-sun-o" aria-hidden="true"></i> 03DIAS / <i class="fa fa-moon-o" aria-hidden="true"></i> 02NOCHES</h3>
                <span class="txt-desc">
                   <i class="fa fa-check" aria-hidden="true"></i> Traslados de entrada y de salida.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 01 noche de alojamiento en Trujillo (01 desayuno).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> HD Templo del Dragón, Chan Chan y balneario de Huanchaco.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> HD Complejo Arqueológico de El Brujo y Museo del Cao.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Bus Turístico Trujillo / Chiclayo.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 01 noche de alojamiento en Chiclayo (01 desayuno).<br>
                   <i class="fa fa-check" aria-hidden="true"></i> HD Tumbas Reales del Señor de Sipán & Huaca Rajada.
               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio">$ 350.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg tour-by-days" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div class="demo-gap">
    <h2 class="tit-ciudad">PUERTO MALDONADO Y IQUITOS</h2>
</div>
<div id="TOUR14" class="jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url(../img/paquetes/iquitos01.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit">PUERTO MALDONADO</h2>
                <h3 class="txt-subTIT-02"><i class="fa fa-sun-o" aria-hidden="true"></i> 03DIAS / <i class="fa fa-moon-o" aria-hidden="true"></i> 02NOCHES</h3>
                <span class="txt-desc">
                   <i class="fa fa-check" aria-hidden="true"></i> 02 noches de alojamiento en Hacienda Concepción.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 02 desayunos, 02 almuerzos, 02 cenas.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Traslados en Puerto Maldonado desde aeropuerto.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Guías naturalistas, bilingües, locales que acompañan al pasajero durante todo su programa. 
               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio">$ 415.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg tour-by-days" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>
<div id="TOUR015" class="jarallax" data-jarallax='{"speed": 0.2}' style="background: #a0836f;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="txt-tit-02">IQUITOS – CEIBA TOPS</h2>
                <h3 class="txt-subTIT-02"><i class="fa fa-sun-o" aria-hidden="true"></i> 03DIAS / <i class="fa fa-moon-o" aria-hidden="true"></i> 02NOCHES</h3>
                <span class="txt-desc-02">
                   <i class="fa fa-check" aria-hidden="true"></i> Traslados terrestre y fluvial aeropuerto/lodge/aeropuerto.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 02 noches de alojamiento en Ceiba Tops.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> 02 desayunos, 03 almuerzos, 02 cenas.<br>
                   <i class="fa fa-check" aria-hidden="true"></i> Servicios incluyen transporte, entradas y guiado en servicio regular (inglés o español).
               </span>
           </div>
           <div class="col-xs-12 col-md-6 pos-precio">
            <span class="txt-desde-02">Precio por persona <strong>DESDE</strong></span>
            <br>
            <span class="txt-precio-02">$ 440.00</span>
            <br>
            <button type="button" class="btn btn-primary btn-lg tour-by-days" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-check-circle" aria-hidden="true"></i> RESERVE AHORA
            </button>
        </div>
    </div>
</div>
</div>


<!-- --------- FORM-PAQUETES --------- -->
<!-- ---------------------------------- -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="color:#546E7A;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel" style="text-align:center;">FORMULARIO DE CONTACTO</h4>
            </div>
            <div class="modal-body" style="padding-bottom: 0px">
                <form id="contactUs" method="POST" action="../process.php">
                    <div class="form-group">
                        <strong style="color: red">*</strong> <span style="color: red">Campos Obligatorios</span>
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="Nombre">Nombres y Apellidos:</label>
                        <input required type="text" name="fullname" class="form-control" id="Nombre" placeholder="Nombres y Apellidos" pattern="[A-Za-z-ñÑ áéíóú]+" title="Porfavor ingrese solo letras">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="Nombre">Email:</label>
                        <input required="" type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="Teléfono">Teléfono:</label>
                        <input required="" type="tel" name="phone" class="form-control" id="Teléfono" placeholder="Teléfono" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="Nacionalidad">Nacionalidad:</label>
                        <input required="" name="nationality" type="text" class="form-control" id="Nacionalidad" placeholder="Nacionalidad">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="tipoDOC">Tipo de Documento:</label>
                        <select required="" name="documentType" id="tipoDOC" class="form-control" placeholder="Tipo de Documento">
                            <option value="">Seleccione Documento</option>
                            <option value="carnet de extranjería">Carnet de Extranjería</option>
                            <option value="dni">DNI</option>
                            <option value="pasaporte">Pasaporte</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="numDOC">Número de Documento:</label>
                        <input required="" type="text" name="documentNumber" class="form-control" id="numDOC" placeholder="Numero de Documento">
                    </div>
                    <div class="form-group">
                        <strong style="color:red">*</strong>
                        <label for="tarjeta">Tipo de Tarjeta:</label>
                        <select placeholder="Tarjeta" required="" name="cardType" id="tarjeta" class="form-control">
                            <option value="">Seleccione Tarjeta</option>
                            <option value="visa">Visa</option>
                            <option value="mastercard">MasterCard</option>
                        </select>
                    </div>
                    <!-- visible solo para paquetes -->
                    <div class="form-group form-dates departureDateForm">
                        <strong style="color:red">*</strong>
                        <label for="">Fecha Inicio :</label>
                        <input value ="18/10/2017" title="Fecha Inicio" type="text" name="departureDateForm" 
                        class="form-control departureDate" maxlength="0">
                    </div>
                    <div class="form-group form-dates returnDateForm">
                        <strong style="color:red">*</strong>
                        <label for="">Fecha Final :</label>
                        <input value="23/10/2017" title="Fecha Final" type="text" name="returnDateForm" 
                        class="form-control returnDate" maxlength="0">
                    </div>
                    <div style="text-align: justify;line-height: 15px; margin-bottom: 9px">
                        <span class="tit-ter-txt">Condiciones Generales:</span>
                        <br>
                        <!-- Este texto solo se muestra cuando es paquete -->
                        <span class="txt-ter-txt condiciones-nohotel">
                           Precios por persona en base a habitación doble. Sujetos a disponibilidad de tarifa y espacios al momento de hacer la reserva. No reembolsables, no endosables ni transferibles. No incluye boletos aéreos. Sujetos a cambio sin previo aviso. No aplica para feriados, ni fin de semana largo. Precios válidos para extranjeros. Validos hasta el 15/12/2017.    
                       </span>
                       <!-- Este texto solo se muestra cuando es hotel -->
                       <span class="txt-ter-txt condiciones-hotel">
                        - Tarifas para pasajeros extranjeros.<br>
                        - Tarifas en dólares americanos.<br>
                        - Tarifas por habitación por noche en habitación Simple / Doble.<br>
                        - Las tarifas incluyen Desayuno en el hotel.<br>
                        - Tarifa especial para el congreso PAAO lima 2017.<br>
                        - Las tarifas incluyen el 10% de servicios del hotel.<br>
                        - Las tarifas NO incluyen el 18% de IGV para pasajeros peruanos.<br>
                        - Pasajeros que no presenten la Tarjeta Andina de Migraciones deberán de asumir el 18% de IGV.<br>
                        - No válida para pasajeros con un mínimo de 60 días de estadía en el Perú.<br>
                        - Todas nuestras habitaciones son NO FUMADORES. De no cumplir con esta restricción, el cliente deberá de asumir una penalidad dispuesta por el hotel.                                
                    </span>


                </div>
                <div class="form-group">
                    <a class="terms" style="cursor: pointer; color: #546E7A !important; font-weight: bold !important;" data-toggle="modal" data-target="#modalPDF">Click para ver los Terminos y Condiciones
                    </a>
                </div>
                <div class="checkbox">
                    <strong style="color:red">*</strong>
                    <label>
                        <input required="" type="checkbox"> Acepto los terminos y condiciones.
                    </label>
                </div>
                <div class="form-group">
                    <div class="col-sm-3">
                        <div class="g-recaptcha-response">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div id="recaptcha" class="g-recaptcha" data-callback="capcha_filled" data-expired-callback="capcha_expired">
                        </div>
                    </div>
                </div>
                <div class="form-group text-center">
                    <button id="booking_now" type="submit" class="btn btn-primary btn_reservar"><i class="fa fa-check-circle" aria-hidden="true"></i> RESERVAR AHORA</button>
                </div>
                <input type="hidden" name="request">
                <input type="hidden" name="typePackage">
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
        </div>
    </div>
</div>
</div>    
<a href="#" class="scroll-to-top">
  <span class="glyphicon glyphicon-arrow-up" style="color: #FFFFFF"></span>
</a>
    <!--
<div class="jarallax frase-txt" data-jarallax='{"speed": 0.2}' style="background-image: url(img/fondo02.png);"></div>
-->
<section id="contact" style="position: relative;background-color: #48321f;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 pie-footer-txt">
                © Costamar Travel 2017. All rights reserved
            </div>
        </div>
    </div>
</section>
<!-- jQuery -->
<script src="../js/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>
<!-- CALENDARIO -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $("#datepicker").datepicker();
    });
</script>
<script>
    $(function() {
        $("#datepicker2").datepicker();
        $("#datepicker3").datepicker();
    });
</script>
<script>
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        autoSize: true,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(function() {
        $("#datepicker").datepicker();
    });
</script>
<script>
    (function($) {

        $(document).ready(

            function() {

                // Comprobar si estamos, al menos, 100 px por debajo de la posición top
                // para mostrar o esconder el botón
                $(window).scroll(function() {

                    if ($(this).scrollTop() > 100) {

                        $('.scroll-to-top').fadeIn();

                    } else {

                        $('.scroll-to-top').fadeOut();

                    }

                });

                // al hacer click, animar el scroll hacia arriba
                $('.scroll-to-top').click(function(e) {

                    e.preventDefault();
                    $('html, body').animate({ scrollTop: 0 }, 800);

                });

            });

    })(jQuery);
</script>
<!---------------------------------->
<!-- SCROOLL -->
<script src="../js/jquery.smooth-scroll.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('a.smoothScroll').smoothScroll({
            offset: -80,
            scrollTarget: $(this).val()
        });
    });
</script>
<!---->
<script src="../js/lightbox.min.js"></script>
<script>
    lightbox.option({
        'albumLabel': "Imagen %1 de %2"
    })
</script>
<script src="../js/main.js"></script>
<div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="color:#546E7A;">
    <div class="modal-dialog" role="document" id="modalPDFContent">
        <div class="modal-content">
            <div class="modal-body">
                <iframe src="http://porcicultura.costamarcorporate.com/terminos-y-condiciones-costamar.pdf" width="100%" height="600"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
            </div>
        </div>
    </div>
</div>
</body>

</html>